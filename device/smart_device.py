# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function

import random
import socket
import sys
import json
import time
import signal
import RPi.GPIO as GPIO

# import RPi.GPIO as GPIO
# import Adafruit_DHT

from colors import bcolors

# DHT_SENSOR_PIN = 4
fan1 = 5
fan2 = 6
fan3 = 13
fan4 = 16
cooling = 19
heater = 21

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(fan1, GPIO.OUT)
GPIO.setup(fan2, GPIO.OUT)
GPIO.setup(fan3, GPIO.OUT)
GPIO.setup(fan4, GPIO.OUT)
GPIO.setup(cooling, GPIO.OUT)
GPIO.setup(heater, GPIO.OUT)
x='{"data": {"heater": "OFF", "fan3": "OFF", "fan1": "ON", "cooling": "ON", "fan2": "OFF", "fan4": "OFF"}, "type": "data"}'
INTERVAL = 10
ADDR = ''
PORT = 10000
# Create a UDP socket
client_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = (ADDR, PORT)
configuration = {}

device_id = sys.argv[1]
if not device_id:
    sys.exit('The device id must be specified.')

print('Bringing up device {}'.format(device_id))


def SendCommand(sock, message, log=True):
    if log:
        print('sending: "{}"'.format(message), file=sys.stderr)

    sock.sendto(message.encode(), server_address)

    # Receive response
    if log:
        print('waiting for response')

    response = sock.recv(4096)

    if log:
        print('received: "{}"'.format(response))

    return response


def MakeMessage(device_id, action, data=''):
    if data:
        return '{{ "device" : "{}", "action":"{}", "data" : "{}" }}'.format(
            device_id, action, data)
    else:
        return '{{ "device" : "{}", "action":"{}" }}'.format(
            device_id, action)


def RunAction(action, data=''):
    global client_sock
    message = MakeMessage(device_id, action, data)
    if not message:
        return
    print('Send data: {} '.format(message))
    event_response = SendCommand(client_sock, message)
    print('Response: {}'.format(event_response))


def UpdateConfiguration(config):
    global configuration
    configuration = config
    sys.stdout.write('\r>> ' + bcolors.OKGREEN + bcolors.BOLD +
                     ' Configuration {}'.format(json.dumps(configuration)) +
                      bcolors.ENDC + ' <<' +
                     '\r>> ' + bcolors.OKBLUE + bcolors.CBLINK +
                     ' Command: no command found' +
                      bcolors.ENDC + ' <<')
    sys.stdout.flush()


def ExecuteCommand(command):
    sys.stdout.write('\r>> ' + bcolors.OKGREEN + bcolors.BOLD +
                     ' Configuration {}'.format(json.dumps(configuration)) +
                      bcolors.ENDC + ' <<' +
                     '\r>> ' + bcolors.OKBLUE + bcolors.CBLINK +
                     ' Command {}'.format(json.dumps(command)) +
                      bcolors.ENDC + ' <<')
    sys.stdout.flush()
    # TODO logic to execute command

def act_relay(number_relay):
    if   number_relay == "fan1":
        relay = fan1
    elif number_relay == "fan2":
        relay = fan2
    elif number_relay == "fan3":
        relay = fan3
    elif number_relay == "fan4":
        relay = fan4
    elif number_relay == "cooling":
        relay = cooling
    elif number_relay == "heater":
        relay = heater
    
    print(number_relay)
    print('\n')
    print('\r')
    if read_json_data["fan1"] == "ON":
      GPIO.output(relay, GPIO.HIGH)
      sys.stdout.write('\r>> ' + bcolors.OKGREEN + bcolors.CBLINK + number_relay +
                       " is ON " + bcolors.ENDC + ' <<')
      sys.stdout.flush()
    elif read_json_data["fan1"] == "OFF":
      GPIO.output(relay, GPIO.LOW)
      sys.stdout.write('\r >>' + bcolors.CRED + bcolors.BOLD + number_relay +
                       "  is OFF " + bcolors.ENDC + ' <<')
      sys.stdout.flush()
def controlRelay():
    global read_json_data
    while True:
        response, server = client_sock.recvfrom(4096) #
        #x='{"data": {"heater": "OFF", "fan3": "OFF", "fan1": "ON", "cooling": "ON", "fan2": "OFF", "fan4": "OFF"}, "type": "data"}'
        print(response)
        if response == ''
            x = json.dumps(response)
        print(x)
        response_json = json.loads(x)
        json_data=response_json["data"]
        print(json_data)
        json_x = json.dumps(json_data)
        print(json_x)
        read_json_data = json.loads(json_x)
        act_relay("fan1")
        act_relay("fan2")
        act_relay("fan3")
        act_relay("fan4")
        

def ReadSensor(interval=INTERVAL):
    while True:
        # h, t = Adafruit_DHT.read_retry(22, DHT_SENSOR_PIN)
        h, t = random.randrange(28, 31), random.randrange(70, 75)
        h = "{:.3f}".format(h)
        t = "{:.3f}".format(t)
        sys.stdout.write(
            '\r >>' + bcolors.CGREEN + bcolors.BOLD +
            'Temp: {}, Hum: {}'.format(t, h) + bcolors.ENDC + ' <<')
        sys.stdout.flush()

        message = MakeMessage(
            device_id, 'event', 'temperature={}, humidity={}'.format(t, h))

        SendCommand(client_sock, message, False)
        time.sleep(interval)


def ListenMessages():
    while True:
        response = client_sock.recv(4096).decode('utf8')
        # print('\nClient received {}'.format(response))
        message = json.loads(response)
        if 'type' in message and message['type'] == 'config':
            UpdateConfiguration(message['data'])
        if 'type' in message and message['type'] == 'command':
            ExecuteCommand(message['data'])


def SignalHandler(signal, frame):
    RunAction('detach')
    print('closing socket', file=sys.stderr)
    client_sock.close()

    sys.exit(0)

signal.signal(signal.SIGINT, SignalHandler)


if sys.argv[2] == 'gateway_listen':
    RunAction('detach')
    RunAction('attach')
    RunAction('subscribe')
    ListenMessages()

elif sys.argv[2] == 'gateway_send':
    random.seed()
    RunAction('event', 'Device is online')
    ReadSensor()
elif sys.argv[2] == 'gateway_subscribe':
    RunAction('detach')
    RunAction('attach')
    RunAction('event', 'LED is online')
    RunAction('subscribe')
    controlRelay()
